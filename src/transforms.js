import * as S from 'sanctuary';
import * as $ from 'sanctuary-def';
import get from 'lodash.get';

// getPath :: String -> Object -> Maybe a
export const getPath = path => x => {
  const y = get(x, path);
  return y == null ? S.Nothing : S.Just(y);
};

export const getPathOr = def => path => S.pipe([getPath(path), S.fromMaybe(def)]);

// isNonPrimitive :: a -> Boolean
export const isNonPrimitive = x => S.or (Array.isArray(x)) (S.is ($.Object) (x));

// isPrimitive :: a -> Boolean
export const isPrimitive = S.unchecked.complement(isNonPrimitive);

// ensureArray :: a -> Array
export const ensureArray = x => S.unchecked.unless (Array.isArray) (S.of (Array)) (x);
