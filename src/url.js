import * as R from 'ramda';

let uglyIdForLogging = 0;

export const fetchCheckedJson = async (url, { refreshTokenFunc, ...options } = {}) => {
  const msgId = uglyIdForLogging;
  uglyIdForLogging += 1;
  console.log(`Will fetch [${msgId}]:`, url);
  const res = await fetch(url, options);
  const { ok, status, statusText } = res;
  console.log(`Response status [${msgId}]:`, status, ok ? 'ok' : 'FAIL');
  const json = res.json ? await res.json() : {};
  if (!ok) {
    if (status === 401 && refreshTokenFunc) {
      return refreshTokenFunc().then(() => fetchCheckedJson(url, options));
    }
    const error = Error(`Fetch failed: ${status} ${statusText}`);
    error.response = { status, statusText };
    return Promise.reject(error);
  }
  console.log(`JSON data [${msgId}]:`, JSON.stringify(json, null, 2));
  return json;
};

export const urlParams = obj =>
  `?${Object.entries(obj)
    .map(pair => pair.join('='))
    .join('&')}`;

export const fromParams = key => {
  const params = new URLSearchParams(window.location.search);
  return params.get(key);
};

export const getHost = () => window.location.host;
export const getProtocol = () => window.location.protocol;
export const isLocalhost = () => getHost().startsWith('localhost');

export const appUrl = (relativePath, params) =>
  `${getProtocol()}//${getHost()}${relativePath}${params ? urlParams(params) : ''}`;

export const sleep = ms => new Promise(r => setTimeout(r, ms));

export const mocked = mock => fn => (isLocalhost() ? (is(Function)(mock) ? mock() : mock) : fn());

export const mockedAsync = async (mock, fnP) =>
  isLocalhost() ? sleep(1000).then(is(Function)(mock) ? mock : () => mock) : fnP();

export const jsToUri = obj => window.btoa(JSON.stringify(obj));

export const uriToJs = str => JSON.parse(window.atob(str));

export const hiddenFormSubmitter = (attr = {}, props = {}) => {
  const form = document.createElement('form');
  Object.entries({ method: 'POST', ...attr }).forEach(pair => form.setAttribute(...pair));
  Object.entries(props).forEach(([name, value]) => {
    const input = document.createElement('input');
    input.setAttribute('type', 'hidden');
    input.setAttribute('name', name);
    input.setAttribute('value', value);
    form.appendChild(input);
  });
  document.body.appendChild(form);
  form.submit();
};

export const createFormData = (props = {}) => {
  const data = new FormData();
  Object.entries(props).forEach(([key, val]) => data.append(key, val));
  return data;
};
