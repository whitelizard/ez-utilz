import jwtDecode from 'jwt-decode';
import { bi } from 'tri-fp';
import { isAfter, fromUnixTime } from 'date-fns/fp';

import { mocked } from './url';
import { loggedInUser } from '../store/mocks';
import { write, remove, read } from './storage';

const safeDecode = bi(jwtDecode);
const safeReadJwtKey = (key, fallback = '') => {
  const [err, result] = safeDecode(read('access_token'));
  return err ? fallback : key === 'email' ? result[key].split(';') : result[key];
};

export const getJwtEmail = () => safeReadJwtKey('email', ['test@test.test']);
export const getEmail = () => mocked(loggedInUser.email)(getJwtEmail);
export const isSender = () => safeReadJwtKey('role-sender', false);
export const getName = () => safeReadJwtKey('name', 'Test Name');

export const getExpiresAt = () => {
  const exp = Number(safeReadJwtKey('exp', 0));
  const fromUnix = fromUnixTime(exp);
  return fromUnix;
};

export const isExpired = () => isAfter(getExpiresAt())(new Date());

export const writeCookie = (name, value) => {
  document.cookie = `${name}=${value};expires=Session;SameSite=Strict;path=/`;
};

export const writeToken = (response) => {
  write('access_token', response.access_token);
  write('refresh_token', response.refresh_token);
  // Temporary workaround to be able to access the document, the
  // DocumentService looks for the signport-token cookie in the browser.
  writeCookie('signport-token', `bearer ${response.access_token}`);
  return response;
};

export const clearTokens = () => {
  remove('access_token');
  remove('refresh_token');
};
