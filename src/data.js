import * as R from 'ramda';
import { createPatch } from 'rfc6902';
import { bi } from 'tri-fp';

/* eslint-disable no-null/no-null */
export const OBJ = Object.freeze(Object.create(null));
/* eslint-enable no-null/no-null */
export const ARR = Object.freeze([]);
export const FUNC = Function.prototype;

const charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
const getChar = () => charset.charAt(Math.floor(Math.random() * charset.length));

export const generateString = (length = 14) =>
  R.pipe(R.range(0), R.reduce(x => `${x}${getChar()}`)(''))(length);

export const idUniquesInFirst = R.differenceWith(
  ({ id: id1 } = OBJ, { id: id2 } = OBJ) => id1 === id2,
);

// isNonPrimitive :: a -> Boolean
export const isNonPrimitive = x => R.or(Array.isArray(x), R.is(Object, x));

// isPrimitive :: a -> Boolean
export const isPrimitive = R.complement(isNonPrimitive);

export const maybeUnjson = x => {
  const [error, parsed] = bi(JSON.parse)(x);
  return error ? x : parsed;
};

export const createStore = (initState = {}, reducer) => {
  /* eslint-disable fp/no-mutation, fp/no-let, functional/no-let */
  let state = initState;
  return {
    getState: key => (key ? state[key] : state),
    dispatch: action => (state = reducer(state, action)),
  };
  /* eslint-enable fp/no-mutation, fp/no-let, functional/no-let */
};

export const adjustCollectionItem = (key = 'id') => (id, adjuster) => items => {
  const idx = R.findIndex(R.propEq(key, id))(items);
  if (idx === -1) return items;
  return R.adjust(idx, adjuster)(items);
};

export const configuredCollectionDiff = ({
  idKey = 'id',
  ignoreKeys,
  skipIf,
  transformAdded,
  transformUpdated,
} = {}) => prev =>
  R.converge(R.mergeLeft, [
    R.reduce(
      (acc, item) => {
        if (skipIf && skipIf(item)) return acc;
        const { [idKey]: id } = item;
        const prevItem = R.find(R.propEq('id', id))(prev);
        if (prevItem) {
          const args = R.when(() => ignoreKeys, R.map(R.omit(ignoreKeys)))([prevItem, item]);
          const patch = createPatch(...args);
          return R.isEmpty(patch)
            ? acc
            : {
                ...acc,
                updated: [
                  ...acc.updated,
                  transformUpdated ? transformUpdated(item, prevItem, patch) : item,
                ],
              };
        }
        return { ...acc, added: [...acc.added, transformAdded ? transformAdded(item) : item] };
      },
      { added: [], updated: [] },
    ),
    R.pipe(idUniquesInFirst(prev), R.objOf('removed')),
  ]);

export const collectionDiff = configuredCollectionDiff();
