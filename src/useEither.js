import { useState, useCallback } from 'react';
import * as R from 'ramda';

const adjustDataIfOk = f => R.when(R.propEq(0, undefined), R.adjust(1, f));

const autoDetermineState = ([error, data]) =>
  error === undefined && data === undefined ? 'loading' : error ? 'error' : 'done';

/**
 * Instead of using real Either monads,
 * this simple custom hook is used to aid with handling error/data.
 *
 * Usage:
 * const { error, data, adjustData, setError, state } = useEither(defaultVal);
 * useEffect(() => {
 *   fetch(...).then(d => adjustData(() => d)).catch(setError);
 * })
 *
 * @param {*} initVal Any value to start with
 * @returns {object} error, data, adjustData, setError & state
 */
export const useEither = initVal => {
  const [pair, setPair] = useState([undefined, undefined]);
  const adjustData = useCallback(f => setPair(adjustDataIfOk(f)), []);
  const setError = useCallback(err => setPair([err, undefined]), []);
  return {
    error: pair[0],
    data: pair[1] ?? initVal,
    adjustData,
    setError,
    state: autoDetermineState(pair),
  };
};
