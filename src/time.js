import { formatDistance, addSeconds } from 'date-fns/fp';
import isFunction from 'lodash.isfunction';

import { R } from './R';

export const secondsToFormattedDistance = S.when(
  S.is($.Number),
  S.pipe([formatDistance(new Date()), x => addSeconds(x, new Date())]),
);

export const utcDateToDate = d =>
  new Date(
    Date.UTC(
      d.getUTCFullYear(),
      d.getUTCMonth(),
      d.getUTCDate(),
      d.getUTCHours(),
      d.getUTCMinutes(),
      d.getUTCSeconds(),
      d.getUTCMilliseconds(),
    ),
  );

export const dateToUTC = d => new Date(d.toUTCString());

const nextPair = ([date, y]) => [addSeconds(R.random(50, 600), date), y + R.random(0, 8) - 4];

export const randData = R.pipe(
  R.defaultTo(1000),
  R.range(0),
  R.reduce(([prev, ...rest]) => [nextPair(prev), prev, ...rest], [[new Date(), 50]]),
  R.reverse,
);

const createInterval = (func, ms) => {
  if (!isFunction(func)) return undefined;
  const id = setInterval(func, ms);
  return () => clearInterval(id);
};
