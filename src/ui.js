import stringify from 'json-stringify-pretty-compact';
import * as R from 'ramda';
import round from 'lodash.round';
import floor from 'lodash.floor';

import { isNonPrimitive } from './transforms';

// withEventValue :: Event a => f -> a -> f
export const withEventValue = fn => ev => fn(ev.target.value);

// withPreventDefault :: Event a => f -> a -> f a
export const withPreventDefault = f => ev => ev.preventDefault() || f(ev);

export const arrayToJson = (arr, options = { maxLength: 100 }) => {
  const str = stringify({ '&&': arr }, options);
  return str
    .replace('"&&":', '')
    .slice(1)
    .slice(0, -1)
    .trim()
    .replace(/\n\s{2}/g, '\n');
};

export const toText = x =>
  isNonPrimitive(x) ? JSON.stringify(x) : x?.toString ? x.toString() : x;

export const unCamel = (x, pre = ' ') => x.replace(/([A-Z])/g, (_, c) => `${pre}${c.toLowerCase()}`);

export const numberDisplay = ({
  precision = 3,
  decimals,
  toFloor = false,
  failTransform = S.I,
  preTransform = S.I,
  postTransform = S.I,
} = {}) => R.ifElse(
    (x) => R.isNil(x) || Number.isNaN(Number(x)),
    failTransform,
    R.compose(
      postTransform,
      (x) => x.toLocaleString(),
      (x) => {
        // Decimals
        if (R.isNil(decimals)) return x;
        const d = R.is(Function, decimals) ? decimals(x) : decimals;
        if (R.isNil(d)) return x;
        return toFloor ? floor(x, d) : round(x, d);
      },
      (x) => {
        // Significant digits / precision
        if (R.isNil(precision)) return x;
        const p = R.is(Function, precision) ? precision(x) : precision;
        if (R.isNil(p)) return x;
        return Number(x.toPrecision(p));
      },
      preTransform,
      Number,
    ),
  );
