import { useState, useRef, useEffect, useCallback } from 'react';

export const useTimeout = (cb, delayMs = 0) => {
  const [isActive, setActive] = useState(false);
  const timer = useRef();
  const savedCb = useRef();

  useEffect(() => {
    savedCb.current = cb;
  }, [cb]);

  const clear = useCallback(() => {
    if (timer.current) clearTimeout(timer.current);
    setActive(false);
  }, []);
  const start = useCallback(() => setActive(true), []);

  const callback = useCallback(() => {
    if (savedCb.current) savedCb.current();
    clear();
  }, [clear]);

  useEffect(() => {
    if (isActive) {
      timer.current = setTimeout(callback, delayMs);
      return () => {
        if (timer.current) clearTimeout(timer.current);
      };
    }
    return Function.prototype;
  }, [callback, delayMs, isActive]);

  return {
    start,
    clear,
    isActive,
  };
};
