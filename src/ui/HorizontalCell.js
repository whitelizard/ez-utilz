import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
const useStyles = makeStyles(theme => ({
  contentElement: {
    display: 'inline-block',
    marginRight: theme.spacing(4),
    marginBottom: theme.spacing(2),
    borderLeft: `solid 3px ${theme.palette.divider}`,
    paddingLeft: theme.spacing(4),
    '&:last-child': { marginRight: 0 },
    [theme.breakpoints.up('md')]: { marginRight: theme.spacing(8) },
  },
  nomargin: { margin: 0 },
}));
export const HorizontalCell = ({ label, content }) => {
  const classes = useStyles();
  return (
    <div className={classes.contentElement}>
      {typeof label === 'string' ? <Typography variant="caption">{label}</Typography> : label}
      {typeof content === 'string' ? (
        <Typography variant="body1">{content}</Typography>
      ) : (
        <Typography component="div" className={classes.nomargin}>
          {content}
        </Typography>
      )}
    </div>
  );
};
HorizontalTableCell.propTypes = {
  content: PropTypes.oneOfType([PropTypes.node, PropTypes.string]).isRequired,
  label: PropTypes.oneOfType([PropTypes.node, PropTypes.string]).isRequired,
};
