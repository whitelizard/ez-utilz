import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() => ({
  iconWrap: {
    display: 'inline-block',
    position: 'relative',
    width: 28,
    height: '0.8rem',
  },
  icon: {
    position: 'absolute',
    top: '-0.3em',
    left: 2,
  },
}));

export const CharIcon = ({ children }) => {
  const classes = useStyles();
  return (
    <div className={classes.iconWrap}>
      <div className={classes.icon}>{children}</div>
    </div>
  );
};

CharIcon.propTypes = {
  children: PropTypes.node,
};
