import {test} from 'tap';

import { unCamel } from '../src/ui-utils';

test('unCamel', t => {
  t.equals(unCamel('myFunc'), 'my func');
  t.equals(unCamel('myFunc', '-'), 'my-func');
  t.plan(2);
});
