import {test} from 'tap';

import { ensureArray, getPathOr, isNonPrimitive, isPrimitive } from '../src/transforms'

test('getPathOr', t => {
  t.equals(getPathOr(0)('a.b')({a:{b:1}}), 1);
  t.equals(getPathOr(0)('a.z')({a:{b:1}}), 0);
  t.plan(2);
});

test('isNonPrimitive', t => {
  t.equals(isNonPrimitive([]), true);
  t.equals(isNonPrimitive({a:2}), true);
  t.equals(isNonPrimitive(Object.create(null)), true);
  t.equals(isNonPrimitive({...Object.create(null)}), true);
  t.equals(isNonPrimitive(['hej', 3, null, {}, {b: 7}, [1,'a',[]]]), true);
  t.equals(isNonPrimitive(7), false);
  t.plan(6);
});

test('isPrimitive', t => {
  t.equals(isPrimitive([]), false);
  t.equals(isPrimitive({a:2}), false);
  t.equals(isPrimitive(Object.create(null)), false);
  t.equals(isPrimitive({...Object.create(null)}), false);
  t.equals(isPrimitive(['hej', 3, null, {}, {b: 7}, [1,'a',[]]]), false);
  t.equals(isPrimitive(7), true);
  t.plan(6);
});

test('ensureArray', t => {
  t.same(ensureArray([]), []);
  t.same(ensureArray({a:2}), [{a:2}]);
  t.same(ensureArray([{a:2}]), [{a:2}]);
  t.same(ensureArray(Object.create(null)), [Object.create(null)]);
  t.same(ensureArray(null), [null]);
  t.plan(5);
});
